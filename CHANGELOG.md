# Change Log

## Version 2

	gpg: Signature made Sun 26 Jan 2020 11:56:22 AEDT
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

Last updated according to www.conorab.com: 2020-01-26

## Version 1

	gpg: Signature made Sun 26 Jan 2020 11:56:09 AEDT
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

Last updated according to www.conorab.com: 2020-01-26
