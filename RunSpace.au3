#include <MsgBoxConstants.au3>
#include <AutoITConstants.au3>

; RunSpace v2

; Copyright 2020 Conor Andrew Buckley

; Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Run("C:\Users\server\steamcmd\spaceengineersdedicatedserver\DedicatedServer64\SpaceEngineersDedicated.exe","C:\Users\server\steamcmd\spaceengineersdedicatedserver\DedicatedServer64")

$FirstWindowAppeared=0
$InstanceSelected=0
$StartCommandSent=0
$ServerStarted=0

While Not DriveMapGet("Z:")

	  DriveMapAdd( "Z:", "\\10.28.25.90\serbians-elmo", $DMA_DEFAULT, "anonymous", "anonymous")
	  Sleep(2000)
WEnd

While 1
	  If WinExists( "Space Engineers - Select Instance of Dedicated server" ) Then
			$FirstWindowAppeared=1
	  EndIf

	  While WinExists( "Space Engineers - Select Instance of Dedicated server" )
			ControlClick("Space Engineers - Select Instance of Dedicated server","","[NAME:button1]")
			$InstanceSelected=1+$InstanceSelected
			sleep(1000)
	  WEnd

	  If WinExists( "Space Engineers - Dedicated Server Manager - Local / Console" ) Then
			If (($FirstWindowAppeared <= 0) Or ($InstanceSelected = 0)) Then
				  MsgBox($MB_OK, "RunSpace", "Stages are being hit out or order; exitting...")
				  Exit
			   Else
				  While Not (ControlGetText("Space Engineers - Dedicated Server Manager - Local / Console", "", "[NAME:lblServerStatus]") = "Running")
						If WinExists( "Space Engineers - Dedicated Server Manager - Local / Console" ) And WinExists("", "SpacePeople") Then
							  ControlClick("Space Engineers - Dedicated Server Manager - Local / Console","","[NAME:startButton]")
							  $StartCommandSent=$StartCommandSent+1
							  Sleep(1000)
						EndIf
						sleep(1000)
				  WEnd
			   EndIf
	  EndIf

	  if ControlGetText("Space Engineers - Dedicated Server Manager - Local / Console", "", "[CLASS:WindowsForms10.STATIC.app.0.3635900_r12_ad1; INSTANCE:32]") = "Running" Then
			if ($StartCommandSent = 0) Then
				  MsgBox($MB_OK, "RunSpace", "The server was started before we issued the commend to start; exitting...")
				  Exit

			   Else
				  $ServerStarted=1
			EndIf
		 EndIf

	  if ($ServerStarted = 1) Then
			MsgBox($MB_OK, "RunSpace", "Server started; exitting...")
			While 1
				  If WinExists( "Space Engineers Dedicated", "Check online for a solution and close the program" ) Then
						WinClose("Space Engineers Dedicated", "Check online for a solution and close the program" )
				  EndIf
				  sleep(60)
			WEnd
	  EndIf

	  sleep(1000)
WEnd
