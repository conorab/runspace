# RunSpace

These scripts are no longer maintained and are provided so others may adapt them for their own purpose. This has no impact on the licence for these scripts.

RunSpace (an AutoIT v3 script) will start a local Space Engineers 1.190.101 console and load the map. This script will also restart the server if it is stopped. In the case of RunSpace version 2, it will also mount an SMB share before attempting to start the server. This can be used with a symlink to store the Space Engineers world on an SMB share. Credentials and a server address for the SMB share have been left in this script, but this is not an issue since the server no longer exists and was restricted via IP address rather than username and password. This was running on a virtual network with each machine in it's own subnet to prevent IP spoofing. The username and password was "anonymous". 
